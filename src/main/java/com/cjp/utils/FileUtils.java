package com.cjp.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

public class FileUtils {
	private static final String DIR="f-";
	public static ThreadLocal<String> currentAppidThread=new ThreadLocal<String>();
	public static ThreadLocal<String> currentAppAppsecretThread=new ThreadLocal<String>();
	public static ThreadLocal<String> currentUser=new ThreadLocal<String>();
	/**
	 * 写文件
	 * @param path
	 * @param content
	 */
	public static void write(String path,String content){
		File file=new File(getDir()+"/"+path);
		write(file, content);
	}
	
	public static void sendWrite(String path,String content,String createTime){
		SimpleDateFormat format=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			Date date=format.parse(createTime);
			format=new SimpleDateFormat("yyyyMMdd");
			createTime=format.format(date);
		} catch (ParseException e1) {
			e1.printStackTrace();
			return;
		}
		File file=new File(getDir(createTime)+"/"+path);
		write(file, content);
	}
	
	/**
	 * 写文件
	 * @param path
	 * @param content
	 */
	public static void write(String path,String createDate,String content){
		File file=new File(getDir(createDate)+"/"+path);
		write(file, content);
	}
	
	/**
	 * 写文件
	 * @param path
	 * @param content
	 */
	public static void write(String dir,String path,String content,boolean isAppend){
		createDir(dir);
		File file=new File(currentUser.get()+"/"+DIR+currentAppidThread.get()+dir+"/"+path);
		if(!file.exists()){
			try {
				file.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		write(file, content,isAppend);
	}
	
	/**
	 * 写文件
	 * @param path
	 * @param content
	 */
	public static void writeApp(String dir,String path,String content,boolean isAppend){
		createAppDir(dir);
		File file=new File(currentUser.get()+"/"+DIR+dir+"/"+path);
		if(!file.exists()){
			try {
				file.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		write(file, content,isAppend);
	}
	
	/**
	 * 写文件
	 * @param path
	 * @param content
	 */
	public static void write(File file,String content,boolean isAppend){
		FileOutputStream fos=null;
		try {
			fos=new FileOutputStream(file,isAppend);
			fos.write(content.getBytes());
			fos.flush();
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				fos.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
     * 写文件
     * @param path
     * @param content
     */
    public static void write(File file,String content){
        write(file, content,false);
    }
    
    /**
     * 读文件
     * @param path
     * @param content
     */
    public static JSONObject read(File file){
      FileInputStream fis=null;
      String content=null;
      try {
          fis=new FileInputStream(file);
          byte[] bytes=new byte[fis.available()];
          fis.read(bytes);
          content=new String(bytes);
          JSONObject jsonObject=JSONObject.parseObject(content);
          return jsonObject;
      } catch (Exception e) {
          e.printStackTrace();
          System.out.println("文件为:"+file.getAbsolutePath()+";内容异常："+content);
      }finally{
          try {
              fis.close();
          } catch (IOException e) {
              e.printStackTrace();
          }
      }
      return null;
    }
	
	public static String getDir(){
		String dir=currentUser.get()+"/"+DIR+currentAppidThread.get()+new SimpleDateFormat("yyyyMMdd").format(new Date());
		File dist=new File(dir);
		if(!dist.exists()){
			dist.mkdir();
		}
		return dir;
	}
	
	public static String getDir(String createDate){
		String dir=currentUser.get()+"/"+DIR+currentAppidThread.get()+createDate;
		File dist=new File(dir);
		if(!dist.exists()){
			dist.mkdir();
		}
		return dir;
	}
	
	public static String createDir(String dir){
		File dist=new File(currentUser.get()+"/"+DIR+currentAppidThread.get()+dir);
		if(!dist.exists()){
			dist.mkdir();
		}
		return dir;
	}
	
	public static String createAppDir(String dir){
		File dist=new File(currentUser.get()+"/"+DIR+dir);
		if(!dist.exists()){
			dist.mkdir();
		}
		return dir;
	}
	
	/**
	 * 读文件
	 * @param path
	 * @param content
	 */
	public static JSONArray read(String dir){
		JSONArray jsonArray=new JSONArray();
		File file=new File(currentUser.get()+"/"+DIR+currentAppidThread.get()+dir);
		if(file.exists()){
			File[] fs=file.listFiles();
			for(File f:fs){
				jsonArray.add(read(f));
			}
			return jsonArray;
		}
		return null;
	}
	
	/**
	 * 读文件
	 * @param path
	 * @param content
	 */
	public static JSONArray readApp(String dir){
		JSONArray jsonArray=new JSONArray();
		File file=new File(currentUser.get()+"/"+DIR+dir);
		if(file.exists()){
			File[] fs=file.listFiles();
			for(File f:fs){
			    jsonArray.add(read(f));
			}
			return jsonArray;
		}
		return null;
	}
	
	/**
	 * 读文件
	 * @param path
	 * @param content
	 */
	public static JSONObject readUser(){
		File file=new File(DIR+"loginUser/userlist");
		if(file.exists()){
		  return read(file);
		}
		return null;
	}
	
	
	/**
	 * 读文件
	 * @param path
	 * @param content
	 */
	public static JSONObject readTemplateApp(String dir,String id){
		File file=new File(currentUser.get()+"/"+DIR+dir);
		if(file.exists()){
			File[] fs=file.listFiles();
			for(File f:fs){
				if(!f.getName().equals(id)){
					continue;
				}
				return read(f);
			}
		}
		return null;
	}
	
	
	/**
	 * 读文件
	 * @param path
	 * @param content
	 */
	public static JSONObject readTemplate(String dir,String id){
		File file=new File(currentUser.get()+"/"+DIR+currentAppidThread.get()+dir);
		if(file.exists()){
			File[] fs=file.listFiles();
			for(File f:fs){
				if(!f.getName().equals(id)){
					continue;
				}
				return read(f);
			}
		}
		return null;
	}
	
	/**
	 * 读文件
	 * @param path
	 * @param content
	 */
	public static File readTemplateFile(String dir,String id){
		File file=new File(currentUser.get()+"/"+DIR+currentAppidThread.get()+dir);
		if(file.exists()){
			File[] fs=file.listFiles();
			for(File f:fs){
				if(!f.getName().equals(id)){
					continue;
				}
				return f;
			}
		}
		return null;
	}
}
